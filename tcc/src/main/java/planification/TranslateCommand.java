package planification;

import geometry.Face;
import geometry.Transform;
import geometry.Vertex;

public class TranslateCommand implements Command {

	private Vertex pivot;
	private Face face;

	public TranslateCommand(Vertex pivot, Face face) {
		super();
		this.pivot = pivot;
		this.face = face;
	}

	@Override
	public void execute() {
		Transform transform = new Transform();
		transform.TranslateTo(pivot, face);
	}
}
