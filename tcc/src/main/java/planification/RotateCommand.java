package planification;

import geometry.Face;
import geometry.Transform;
import geometry.Vertex;

public class RotateCommand implements Command {

	private double angle;
	private Vertex axis;
	private Face face;

	public RotateCommand(double angle, Vertex axis, Face face) {
		super();
		this.angle = angle;
		this.axis = axis;
		this.face = face;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public Vertex getAxis() {
		return axis;
	}

	public void setAxis(Vertex axis) {
		this.axis = axis;
	}

	public Face getFace() {
		return face;
	}

	public void setFace(Face face) {
		this.face = face;
	}

	@Override
	public void execute() {
		Transform transform = new Transform();

		if (angle != 0 && angle != Math.PI && !axis.isZero()) {
			transform.RotateAroundAxis(axis, -angle, face);
		}
	}
}
