package planification;

public interface Command {

	void execute();
}
