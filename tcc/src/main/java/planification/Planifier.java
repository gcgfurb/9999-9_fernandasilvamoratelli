package planification;

import geometry.Face;
import geometry.Functions;
import geometry.Polyhedra;
import geometry.Transform;
import geometry.Vertex;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import javax.media.opengl.GL2;

import bfs.Color;
import bfs.Graph;
import bfs.GraphNode;

public class Planifier {

	private Polyhedra polyhedra;

	public Planifier(Polyhedra polyhedra) {
		this.polyhedra = polyhedra;
	}

	public void Run() {

		// TODO: construir grafo
		Graph graph = new Graph();

		for (Face f : polyhedra.getFaces()) {

		}

		Polyhedra planarNet = BFS(graph);

		for (GraphNode node : graph.getNodes()) {

			while (node.peekTransformation() != null) {
				Command c = node.pollTransformation();

				c.execute();
			}
		}
	}

	public Polyhedra BFS(Graph graph) {

		Polyhedra planarNet = new Polyhedra();
		Queue<GraphNode> queue = new LinkedList<GraphNode>();

		Set<GraphNode> nodes = graph.getNodes();

		if (nodes != null && nodes.size() > 0) {

			GraphNode s = nodes.iterator().next();
			nodes.remove(s);

			for (GraphNode gn : nodes) {

				gn.setColor(Color.WHITE);
				gn.setDistance(Integer.MAX_VALUE);
				gn.setPredecessor(null);
			}

			s.setColor(Color.GRAY);
			s.setDistance(0);
			s.setPredecessor(null);

			queue.add(s);

			while (queue.size() > 0) {
				GraphNode u = queue.poll();

				for (GraphNode adj : u.getAdjacencyList()) {

					if (adj.getColor() == Color.WHITE) {
						adj.setColor(Color.GRAY);
						adj.setDistance(u.getDistance() + 1);
						adj.setPredecessor(u);

						queue.add(adj);
					}
				}

				u.setColor(Color.BLACK);

				processFace(planarNet, u);
			}
		}

		return planarNet;
	}

	private void processFace(Polyhedra planarNet, GraphNode u) {
		Face f = u.getFace();
		Vertex axis = new Vertex();
		double angle = 0;

		Transform transform = new Transform();

		if (u.getPredecessor() != null) {

			System.out.println("Processing FACE: " + f.getFaceID());
			System.out.println("Predecessor: "
					+ u.getPredecessor().getFace().getFaceID());

			Face aux = u.getPredecessor().getFace();
			Face newFace = planarNet.getFaceFromId(aux.getFaceID());

			axis = Functions.crossProduct(f.getNormalVector(), u
					.getPredecessor().getFace().getNormalVector());

			angle = Functions.getAngle(f.getNormalVector(), u.getPredecessor()
					.getFace().getNormalVector());

			Vertex commonVertex = f.getVertexInCommon(
					u.getPredecessor().getFace()).clone();
			transform.TranslateTo(new Vertex(-commonVertex.getX(),
					-commonVertex.getY(), -commonVertex.getZ()), f);

			if (angle != 0 && angle != Math.PI && !axis.isZero()) {
				transform.RotateAroundAxis(axis, -angle, f);

				u.setRotation(new RotateCommand(angle, axis, f));
			}

			transform.TranslateTo(commonVertex, f);

			if (u.getPredecessor().getRotation() != null) {
				RotateCommand r = (RotateCommand) u.getPredecessor()
						.getRotation();

				commonVertex = f
						.getVertexInCommon(u.getPredecessor().getFace())
						.clone();
				transform.TranslateTo(new Vertex(-commonVertex.getX(),
						-commonVertex.getY(), -commonVertex.getZ()), f);

				transform.RotateAroundAxis(r.getAxis(), -r.getAngle(), f);

				Vertex v = newFace.getVertexFromID(commonVertex.getVertexID())
						.clone();
				transform.TranslateTo(new Vertex(v.getX(), v.getY(), v.getZ()),
						f);
			}
		}

		planarNet.addFace(f);
	}
}
