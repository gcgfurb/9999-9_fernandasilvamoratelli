package planification;

public class UnfoldCommand implements Command {

	private Command initialTranslation;
	private Command rotation;
	private Command finalTranslation;

	public UnfoldCommand(Command initialTranslation, Command rotation,
			Command finalTranslation) {
		super();
		this.initialTranslation = initialTranslation;
		this.rotation = rotation;
		this.finalTranslation = finalTranslation;
	}

	@Override
	public void execute() {

		this.initialTranslation.execute();
		this.rotation.execute();
		this.finalTranslation.execute();
	}
}
