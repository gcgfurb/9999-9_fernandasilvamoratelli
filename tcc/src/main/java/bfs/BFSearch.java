package bfs;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class BFSearch {

	public static void BFS(Graph graph) {

		Queue<GraphNode> queue = new LinkedList<GraphNode>();

		Set<GraphNode> nodes = graph.getNodes();

		if (nodes != null && nodes.size() > 0) {

			GraphNode s = nodes.iterator().next();
			nodes.remove(s);

			for (GraphNode gn : nodes) {

				gn.setColor(Color.WHITE);
				gn.setDistance(Integer.MAX_VALUE);
				gn.setPredecessor(null);
			}

			s.setColor(Color.GRAY);
			s.setDistance(0);
			s.setPredecessor(null);

			queue.add(s);

			while (queue.size() > 0) {
				GraphNode u = queue.poll();

				for (GraphNode adj : u.getAdjacencyList()) {

					if (adj.getColor() == Color.WHITE) {
						adj.setColor(Color.GRAY);
						adj.setDistance(u.getDistance() + 1);
						adj.setPredecessor(u);

						queue.add(adj);
					}
				}

				u.setColor(Color.BLACK);
			}
		}
	}
}
