package bfs;
import geometry.Face;

import java.util.HashSet;
import java.util.Set;

public class Graph {

	private Set<GraphEdge> edges;
	private Set<GraphNode> nodes;

	public Graph() {
		this.edges = new HashSet<GraphEdge>();
		this.nodes = new HashSet<GraphNode>();
	}

	public Set<GraphEdge> getEdges() {
		return edges;
	}

	public void setEdges(Set<GraphEdge> edges) {
		this.edges = edges;
	}

	public Set<GraphNode> getNodes() {
		return nodes;
	}

	public void setNodes(Set<GraphNode> nodes) {
		this.nodes = nodes;
	}

	public void addEdge(GraphNode source, GraphNode target) {

		GraphEdge edge = new GraphEdge(source, target);

		if (!edgeExists(edge)) {
			edges.add(edge);
		}
	}

	public void addEdge(GraphEdge edge) {

		if (!edgeExists(edge)) {
			edges.add(edge);
		}
	}

	public boolean edgeExists(GraphEdge edge) {

		for (GraphEdge e : edges) {
			if (e.getSource().equals(edge.getSource())
					&& e.getTarget().equals(edge.getTarget())) {
				return true;
			}
		}

		return false;
	}

	public boolean nodeExists(GraphNode node) {

		for (GraphNode n : nodes) {
			if (n.equals(node)) {
				return true;
			}
		}

		return false;
	}

	public void addNode(GraphNode node) {

		if (!nodeExists(node)) {
			nodes.add(node);
		}
	}

	public void addNode(Face face) {

		GraphNode node = new GraphNode(face);

		if (!nodeExists(node)) {
			nodes.add(node);
		}
	}
}
