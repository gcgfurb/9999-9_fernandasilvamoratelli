package bfs;
public class GraphEdge {

	private GraphNode source;
	private GraphNode target;

	public GraphEdge() {

	}

	public GraphEdge(GraphNode source, GraphNode target) {

		this.source = source;
		this.target = target;
		
		this.source.addAdjacentNode(target);
		this.target.addAdjacentNode(source);
	}

	public GraphNode getSource() {
		return source;
	}

	public void setSource(GraphNode source) {
		this.source = source;
	}

	public GraphNode getTarget() {
		return target;
	}

	public void setTarget(GraphNode target) {
		this.target = target;
	}

}
