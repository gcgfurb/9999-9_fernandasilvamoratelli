package bfs;

import geometry.Face;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import planification.Command;

public class GraphNode {

	private Face face;

	private int distance;
	private GraphNode predecessor;
	private Color color;
	private int predecessorEdgeId;

	private Set<GraphNode> adjacencyList;

	private Command rotation;

	private Queue<Command> transformations;

	public Command getRotation() {
		return rotation;
	}

	public void setRotation(Command rotation) {
		this.rotation = rotation;
	}

	public GraphNode() {
		this.distance = Integer.MAX_VALUE;
		this.predecessor = null;
		this.color = Color.WHITE;

		this.adjacencyList = new HashSet<GraphNode>();

		this.predecessorEdgeId = 0;
		this.rotation = null;

		transformations = new LinkedList<Command>();
	}

	public GraphNode(Face face) {
		this.distance = Integer.MAX_VALUE;
		this.predecessor = null;
		this.color = Color.WHITE;

		this.adjacencyList = new HashSet<GraphNode>();

		this.face = face;
		this.rotation = null;

		transformations = new LinkedList<Command>();
	}

	public void addTransformation(Command command) {
		transformations.add(command);
	}

	public void addTransformations(LinkedList<Command> commands) {
		transformations.addAll(commands);
	}

	public Command pollTransformation() {
		return transformations.poll();
	}

	public Command peekTransformation() {
		return transformations.peek();
	}

	public void addAdjacentNode(GraphNode adj) {

		if (!adjacentnodeExists(adj)) {
			adjacencyList.add(adj);
		}
	}

	private boolean adjacentnodeExists(GraphNode node) {

		for (GraphNode n : adjacencyList) {
			if (n.equals(node)) {
				return true;
			}
		}

		return false;
	}

	public int getPredecessorEdgeId() {
		return predecessorEdgeId;
	}

	public void setPredecessorEdgeId(int predecessorEdgeId) {
		this.predecessorEdgeId = predecessorEdgeId;
	}

	public Set<GraphNode> getAdjacencyList() {
		return adjacencyList;
	}

	public void setAdjacencyList(Set<GraphNode> adjacencyList) {
		this.adjacencyList = adjacencyList;
	}

	public Face getFace() {
		return face;
	}

	public void setFace(Face face) {
		this.face = face;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public GraphNode getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(GraphNode predecessor) {
		this.predecessor = predecessor;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		final GraphNode other = (GraphNode) obj;

		if (this.face != null && !this.face.equals(other.getFace())) {
			return false;
		}

		return true;
	}
}
