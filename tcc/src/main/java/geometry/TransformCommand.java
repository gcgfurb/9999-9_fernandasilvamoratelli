package geometry;

public abstract class TransformCommand {

	private Transform transform;
	private Face face;

	public TransformCommand(Transform transform, Face face) {
		this.transform = transform;
		this.face = face;
	}

	public void execute() {

	}
}
