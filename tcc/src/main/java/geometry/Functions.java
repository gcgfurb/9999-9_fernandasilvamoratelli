package geometry;

public class Functions {

	public static double dotProduct(Vertex v1, Vertex v2) {

		double[] a = { v1.getX(), v1.getY(), v1.getZ() };
		double[] b = { v2.getX(), v2.getY(), v2.getZ() };

		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * b[i];
		}

		return sum;
	}

	public static Vertex crossProduct(Vertex v1, Vertex v2) {
		Double newA = v1.getY() * v2.getZ() - v1.getZ() * v2.getY();
		Double newB = v1.getZ() * v2.getX() - v1.getX() * v2.getZ();
		Double newC = v1.getX() * v2.getY() - v1.getY() * v2.getX();

		System.out.println("Normal 1: " + v1.getX() + ", " + v1.getY() + ", "
				+ v1.getZ());

		System.out.println("Normal 2: " + v2.getX() + ", " + v2.getY() + ", "
				+ v2.getZ());

		System.out.println("Cross: " + newA + ", " + newB + ", " + newC);

		return new Vertex(newA, newB, newC);
	}

	public static double getAngle(Vertex normalVector1, Vertex normalVector2) {

		normalVector1 = normalVector1.normalize();
		normalVector2 = normalVector2.normalize();

		double product = Functions.dotProduct(normalVector1, normalVector2);
		double magNormalV = Math.sqrt(Functions.dotProduct(normalVector1,
				normalVector1));
		double magPlaneNV = Math.sqrt(Functions.dotProduct(normalVector2,
				normalVector2));

		return Math.acos(product / (magNormalV * magPlaneNV));
	}
}
