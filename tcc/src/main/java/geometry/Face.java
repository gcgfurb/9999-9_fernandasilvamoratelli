package geometry;

import java.util.LinkedHashSet;
import java.util.Set;

public class Face {

	private int faceID;
	private String description;

	private Transform transform;

	private Set<Vertex> vertices = new LinkedHashSet<Vertex>();
	private Set<Edge> edges = new LinkedHashSet<Edge>();
	private Set<Face> adjFaces = new LinkedHashSet<Face>();
	private Vertex NormalVector = new Vertex(0.0, 0.0, 0.0);

	public void setNormalVector(Vertex normalVector) {
		NormalVector = normalVector;
	}

	public Face(int faceID) {

		this.transform = new Transform();

		this.faceID = faceID;
	}

	public Face(int faceID, Set<Vertex> vertices) {

		this.transform = new Transform();

		this.faceID = faceID;
		this.vertices = vertices;

		this.generateEdges();
	}

	public void addVertex(Vertex vertex) {

		vertices.add(vertex.clone());

		this.generateEdges();
	}

	private void generateEdges() {
		edges.clear();

		Vertex[] vertexArray = new Vertex[vertices.size()];
		vertices.toArray(vertexArray);

		for (int i = 0; i < vertexArray.length - 1; i++) {
			Edge e = new Edge(vertexArray[i], vertexArray[i + 1]);

			edges.add(e);
		}

		Edge e = new Edge(vertexArray[vertexArray.length - 1], vertexArray[0]);

		edges.add(e);
	}

	public Vertex getVertexInCommon(Face other) {
		for (Vertex v : this.getVertices()) {
			for (Vertex u : other.getVertices()) {
				if (v.getVertexID() == u.getVertexID()) {
					return v;
				}
			}
		}

		return null;
	}

	public Edge getEdgeInCommon(Face other) {
		for (Edge e : other.getEdges()) {
			if (this.edges.contains(e)) {
				return e;
			}
		}

		for (Edge e : other.getEdges()) {
			for (Edge e2 : this.edges) {
				if (e.isEquivalent(e2)) {
					return e;
				}
			}
		}

		return null;
	}

	public Vertex getFirstVertex() {
		for (Vertex v : this.vertices) {
			return v;
		}

		return null;
	}

	public Vertex getVertexFromID(int id) {
		for (Vertex v : this.vertices) {
			if (v.getVertexID() == id) {
				return v;
			}
		}

		return null;
	}

	public Vertex getClosestToOrigin() {

		Vertex origin = new Vertex(0, 0, 0);
		Vertex closest = null;
		double distance = Double.MAX_VALUE;

		for (Vertex v : this.vertices) {
			if (v.getDistanceTo(origin) < distance) {
				distance = v.getDistanceTo(origin);
				closest = v;
			}
		}

		return closest;
	}

	public Vertex calculateFaceNormal() {

		Vertex normal = new Vertex(0, 0, 0);
		Vertex first = new Vertex(0, 0, 0);
		Vertex second = new Vertex(0, 0, 0);

		Vertex[] vertexArray = new Vertex[vertices.size()];
		vertices.toArray(vertexArray);

		for (int i = 0; i < vertexArray.length - 1; i++) {
			Vertex current = vertexArray[i];
			Vertex next = vertexArray[i + 1];

			if (i == 0) {
				first = current.subtract(next);
			} else if (i == 1) {
				second = current.subtract(next);
			} else {
				break;
			}
		}

		normal = first.crossProduct(second);

		if (normal.isZero()) {
			return normal;
		} else {
			return normal.normalize();
		}
	}

	public int getFaceID() {
		return faceID;
	}

	public void setFaceID(int faceID) {
		this.faceID = faceID;
	}

	public Set<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Set<Vertex> vertices) {
		this.vertices = vertices;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}

	public Set<Face> getAdjFaces() {
		return adjFaces;
	}

	public void setAdjFaces(Set<Face> adjFaces) {
		this.adjFaces = adjFaces;
	}

	public Transform getTransform() {
		return transform;
	}

	public void setTransform(Transform transform) {
		this.transform = transform;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Vertex getNormalVector() {
		return NormalVector;
	}

	public Edge getEdgeFromId(int id) {
		for (Edge e : edges) {
			if (e.getId() == id) {
				return e;
			}
		}

		return null;
	}
}
