package geometry;

import java.util.LinkedHashSet;
import java.util.Set;

public class Edge {

	private int id;
	
	private Vertex source;
	private Vertex target;

	Set<Face> incidentFaces = new LinkedHashSet<Face>();

	public Edge(Vertex source, Vertex target) {

		this.source = source.clone();
		this.target = target.clone();
	}

	public void addIncidentFace(Face f) {
		incidentFaces.add(f);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	public boolean isEquivalent(Edge other) {
		if (source.equals(other.target) && target.equals(other.source)) {
			return true;
		}
		
		return false;
	}

	public Vertex getSource() {
		return source;
	}

	public void setSource(Vertex source) {
		this.source = source;
	}

	public Vertex getTarget() {
		return target;
	}

	public void setTarget(Vertex target) {
		this.target = target;
	}
	
	public Set<Vertex> getVertices() {
		Set<Vertex> vertices = new LinkedHashSet<Vertex>();
		vertices.add(source);
		vertices.add(target);
		
		return vertices;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
