package geometry;

import java.util.LinkedHashSet;
import java.util.Set;

public class Polyhedra {

	private Set<Face> faces = new LinkedHashSet<Face>();
	private Set<Edge> edges = new LinkedHashSet<Edge>();
	private Set<Vertex> vertices = new LinkedHashSet<Vertex>();

	public Polyhedra() {
	}

	public Polyhedra(LinkedHashSet<Face> faces) {
		this.faces = faces;

		this.generateStructure();
	}

	public void addVertex(Vertex v) {

		v.setX(vertices.size() + 1);
		vertices.add(v);
	}

	public void addVertex(int x, int y, int z) {

		int vertexID = vertices.size() + 1;
		Vertex v = new Vertex(vertexID, x, y, z);
		vertices.add(v);
	}

	public void addFace(LinkedHashSet<Vertex> vertices) {

		int faceID = faces.size() + 1;
		Face f = new Face(faceID, vertices);

		faces.add(f);
	}

	public void addFace(Face face) {

		faces.add(face);
	}

	public Face getFaceFromId(int id) {
		for (Face f : faces) {
			if (f.getFaceID() == id) {
				return f;
			}
		}

		return null;
	}

	private void generateStructure() {
		edges.clear();

		for (Face f : faces) {
			for (Edge e : f.getEdges()) {

				if (!this.edges.contains(e)) {

					boolean isEquivalent = false;

					for (Edge edge : this.edges) {
						if (edge.isEquivalent(e)) {
							isEquivalent = true;
							break;
						}
					}

					if (!isEquivalent) {
						edges.add(e);
					}
				}

				e.addIncidentFace(f);
			}

			for (Vertex v : f.getVertices()) {
				if (!this.vertices.contains(v)) {
					this.vertices.add(v);
				}
			}
		}
	}

	public Set<Face> getFaces() {
		return faces;
	}

	public void setFaces(Set<Face> faces) {
		this.faces = faces;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}

	public Set<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Set<Vertex> vertices) {
		this.vertices = vertices;
	}
}
