package geometry;

public class Vertex {

	private int vertexID;

	private double x;
	private double y;
	private double z;
	private double w;

	public Vertex() {
		this(0, 0, 0, 0);
	}

	public Vertex(int vertexID, double x, double y, double z) {

		this.vertexID = vertexID;
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 1;
	}

	public Vertex(double x, double y, double z) {

		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 1;
	}

	public Vertex(double x, double y, double z, double w) {

		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Vertex normalize() {

		double t = Math.sqrt(x * x + y * y + z * z);

		Vertex v = new Vertex(this.x / t, this.y / t, this.z / t);

		return v;
	}

	public void scale(double sc) {
		x *= sc;
		y *= sc;
		z *= sc;
	}

	public double dotProduct(Vertex v2) {

		double[] a = { this.x, this.y, this.z };
		double[] b = { v2.getX(), v2.getY(), v2.getZ() };

		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * b[i];
		}

		return sum;
	}

	public Vertex crossProduct(Vertex v2) {
		Double newX = this.y * v2.getZ() - this.z * v2.getY();
		Double newY = this.z * v2.getX() - this.x * v2.getZ();
		Double newZ = this.x * v2.getY() - this.y * v2.getX();

		return new Vertex(newX, newY, newZ);
	}

	public Vertex subtract(Vertex v2) {
		Double newX = this.x - v2.getX();
		Double newY = this.y * v2.getY();
		Double newZ = this.z * v2.getZ();

		return new Vertex(newX, newY, newZ);
	}

	public Boolean isParallel(Vertex u) {

		double kX, kY, kZ = 0;

		kX = this.x / u.getX() == 0 ? 1 : u.getX();
		kY = this.y / u.getY() == 0 ? 1 : u.getY();
		kZ = this.z / u.getZ() == 0 ? 1 : u.getZ();

		return kX == kY && kY == kZ;
	}

	public Boolean isZero() {
		return Math.abs(this.x) == 0 && Math.abs(this.y) == 0 && Math.abs(this.z) == 0;
	}

	public double getDistanceTo(Vertex other) {

		double distance = Math.pow(this.x - other.getX(), 2)
				+ Math.pow(this.y - other.getY(), 2)
				+ Math.pow(this.z - other.getZ(), 2);

		return Math.sqrt(distance);
	}

	public Vertex clone() {
		return new Vertex(this.vertexID, this.x, this.y, this.z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vertexID;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (vertexID != other.vertexID)
			return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}

	public int getVertexID() {
		return vertexID;
	}

	public void setVertexID(int vertexID) {
		this.vertexID = vertexID;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}
}
