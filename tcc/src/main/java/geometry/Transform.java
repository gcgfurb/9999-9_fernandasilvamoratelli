package geometry;

/**
 * Classe para representar a Matriz de Transforma��o
 * 
 * @author Fernanda Moratelli
 * 
 */
public class Transform {

	public static final double RAS_DEG_TO_RAD = 0.017453292519943295769236907684886;

	private double[] matrix = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	/**
	 * Inicia os valores da Matriz para que ela seja uma matriz identidade
	 */
	public void MakeIdentity() {
		for (int i = 0; i < 16; ++i) {
			matrix[i] = 0.0;
		}

		matrix[0] = matrix[5] = matrix[10] = matrix[15] = 1.0;
	}

	/**
	 * Faz a transla��o
	 * 
	 * @param translationVector
	 *            Ponto com os valores para a transla��o
	 */
	public void MakeTranslation(Vertex translationVector) {
		MakeIdentity();
		matrix[12] = translationVector.getX();
		matrix[13] = translationVector.getY();
		matrix[14] = translationVector.getZ();
	}

	/**
	 * Faz a rota��o X
	 * 
	 * @param radians
	 *            Grau em radianos
	 */
	public void MakeXRotation(double radians) {
		MakeIdentity();
		matrix[5] = Math.cos(radians);
		matrix[9] = -Math.sin(radians);
		matrix[6] = Math.sin(radians);
		matrix[10] = Math.cos(radians);
	}

	/**
	 * Faz a rota��o Y
	 * 
	 * @param radians
	 *            Grau em radianos
	 */
	public void MakeYRotation(double radians) {
		MakeIdentity();
		matrix[0] = Math.cos(radians);
		matrix[8] = Math.sin(radians);
		matrix[2] = -Math.sin(radians);
		matrix[10] = Math.cos(radians);
	}

	/**
	 * Faz a rota��o Z
	 * 
	 * @param radians
	 *            Grau em radianos
	 */
	public void MakeZRotation(double radians) {
		MakeIdentity();
		matrix[0] = Math.cos(radians);
		matrix[4] = -Math.sin(radians);
		matrix[1] = Math.sin(radians);
		matrix[5] = Math.cos(radians);
	}

	public void Initialize() {
		for (int i = 0; i < 16; ++i) {
			matrix[i] = 0.0;
		}
	}

	public void RotateAroundAxis(Vertex axis, double angle, Face face) {

		System.out.println("Rotate");
		// System.out.println("Angle: " + Math.toDegrees(angle));
		System.out.println("Angle: " + angle);
		System.out.println("Axis: " + axis.getX() + ", " + axis.getY() + ", "
				+ axis.getZ());

		Initialize();

		double cosseno = Math.round(Math.cos(angle));
		double seno = Math.round(Math.sin(angle));

		matrix[0] = cosseno + Math.pow(axis.getX(), 2) * (1 - cosseno);
		matrix[4] = axis.getY() * axis.getX() * (1 - cosseno) + axis.getZ()
				* seno;
		matrix[8] = axis.getZ() * axis.getX() * (1 - cosseno) - axis.getY()
				* seno;

		matrix[1] = axis.getX() * axis.getY() * (1 - cosseno) - axis.getZ()
				* seno;
		matrix[5] = cosseno + Math.pow(axis.getY(), 2) * (1 - cosseno);
		matrix[9] = axis.getZ() * axis.getY() * (1 - cosseno) + axis.getX()
				* seno;

		matrix[2] = axis.getX() * axis.getZ() * (1 - cosseno) + axis.getY()
				* seno;
		matrix[6] = axis.getY() * axis.getZ() * (1 - cosseno) - axis.getX()
				* seno;
		matrix[10] = cosseno + Math.pow(axis.getZ(), 2) * (1 - cosseno);

		// for (Edge e : face.getEdges()) {
		//
		// Vertex vetor = e.getSource().subtract(e.getTarget());
		//
		// if (!vetor.isParallel(axis)) {
		// for (Vertex v : e.getVertices()) {
		// Vertex v2 = new Vertex(matrix[0] * v.getX() + matrix[4]
		// * v.getY() + matrix[8] * v.getZ() + matrix[12]
		// * v.getW(), matrix[1] * v.getX() + matrix[5]
		// * v.getY() + matrix[9] * v.getZ() + matrix[13]
		// * v.getW(), matrix[2] * v.getX() + matrix[6]
		// * v.getY() + matrix[10] * v.getZ() + matrix[14]
		// * v.getW(), matrix[3] * v.getX() + matrix[7]
		// * v.getY() + matrix[11] * v.getZ() + matrix[15]
		// * v.getW());
		//
		// face.getVertexFromID(v.getVertexID()).setX(v2.getX());
		// face.getVertexFromID(v.getVertexID()).setY(v2.getY());
		// face.getVertexFromID(v.getVertexID()).setZ(v2.getZ());
		// }
		// }
		// }

		for (Vertex v : face.getVertices()) {

			Vertex v2 = new Vertex(matrix[0] * v.getX() + matrix[4] * v.getY()
					+ matrix[8] * v.getZ() + matrix[12] * v.getW(), matrix[1]
					* v.getX() + matrix[5] * v.getY() + matrix[9] * v.getZ()
					+ matrix[13] * v.getW(), matrix[2] * v.getX() + matrix[6]
					* v.getY() + matrix[10] * v.getZ() + matrix[14] * v.getW(),
					matrix[3] * v.getX() + matrix[7] * v.getY() + matrix[11]
							* v.getZ() + matrix[15] * v.getW());

			v.setX(v2.getX());
			v.setY(v2.getY());
			v.setZ(v2.getZ());
		}
	}

	public void TranslateTo(Vertex translationVector, Face face) {

		System.out.println("Translate");
		System.out.println(translationVector.getX() + ", "
				+ translationVector.getY() + ", " + translationVector.getZ());

		MakeIdentity();
		matrix[12] = translationVector.getX();
		matrix[13] = translationVector.getY();
		matrix[14] = translationVector.getZ();

		for (Vertex v : face.getVertices()) {
			Vertex v2 = new Vertex(v.getX() + matrix[12],
					v.getY() + matrix[13], v.getZ() + matrix[14]);

			System.out.println("*******");
			System.out.println(v.getX() + ", " + v.getY() + ", " + v.getZ());
			System.out.println(v2.getX() + ", " + v2.getY() + ", " + v2.getZ());
			v.setX(v2.getX());
			v.setY(v2.getY());
			v.setZ(v2.getZ());
		}

		System.out.println("trans*******");
	}

	/**
	 * Altera a escala
	 * 
	 * @param sX
	 *            Valor de x
	 * @param sY
	 *            Valor de y
	 * @param sZ
	 *            Valor de z
	 */
	public void MakeScale(double sX, double sY, double sZ) {
		MakeIdentity();
		matrix[0] = sX;
		matrix[5] = sY;
		matrix[10] = sZ;
	}

	/**
	 * Transforma um ponto
	 * 
	 * @param ponto
	 *            Ponto com os par�metros
	 * @return Ponto transformado
	 */
	public Vertex transformPoint(Vertex ponto) {
		Vertex pointResult = new Vertex(matrix[0] * ponto.getX() + matrix[4]
				* ponto.getY() + matrix[8] * ponto.getZ() + matrix[12]
				* ponto.getW(), matrix[1] * ponto.getX() + matrix[5]
				* ponto.getY() + matrix[9] * ponto.getZ() + matrix[13]
				* ponto.getW(), matrix[2] * ponto.getX() + matrix[6]
				* ponto.getY() + matrix[10] * ponto.getZ() + matrix[14]
				* ponto.getW(), matrix[3] * ponto.getX() + matrix[7]
				* ponto.getY() + matrix[11] * ponto.getZ() + matrix[15]
				* ponto.getW());
		return pointResult;
	}

	/**
	 * Transforma uma matriz
	 * 
	 * @param t
	 *            Matriz de Transforma��o
	 * @return Nova matriz de transforma��o
	 */
	public Transform transformMatrix(Transform t) {
		Transform result = new Transform();
		for (int i = 0; i < 16; ++i)
			result.matrix[i] = matrix[i % 4] * t.matrix[i / 4 * 4]
					+ matrix[(i % 4) + 4] * t.matrix[i / 4 * 4 + 1]
					+ matrix[(i % 4) + 8] * t.matrix[i / 4 * 4 + 2]
					+ matrix[(i % 4) + 12] * t.matrix[i / 4 * 4 + 3];
		return result;
	}

	/**
	 * Obt�m um elemento
	 * 
	 * @param index
	 *            �ndice do elemento
	 * @return Elemento da matriz
	 */
	public double GetElement(int index) {
		return matrix[index];
	}

	/**
	 * Define um elemento
	 * 
	 * @param index
	 *            �ndice do elemento
	 * @param value
	 *            Valor do elemento
	 */
	public void SetElement(int index, double value) {
		matrix[index] = value;
	}

	/**
	 * Busca os dados da matriz
	 * 
	 * @return Dados da matriz
	 */
	public double[] GetData() {
		return matrix;
	}

	/**
	 * Define os dados da matriz
	 * 
	 * @param data
	 *            Vetor com os dados
	 */
	public void SetData(double[] data) {
		int i;

		for (i = 0; i < 16; i++) {
			matrix[i] = (data[i]);
		}
	}
}
