package dcel;

public class HalfEdge {

	// Origin of the edge
	private Vertex origin;

	private HalfEdge twin;

	// Next and previous HalfEdges in counterclowise order
	private HalfEdge next;
	private HalfEdge previous;

	// Face that lies to the edge's left for an observer walking along the edge
	private Face incidentFace;

	public HalfEdge(Vertex origin) {
		this.origin = origin;

		origin.setIncidentEdge(this);
	}

	public HalfEdge(Vertex origin, HalfEdge twin, Face incidentFace) {
		this.origin = origin;
		this.twin = twin;
		this.incidentFace = incidentFace;

		origin.setIncidentEdge(this);
	}

	public HalfEdge(Vertex origin, Vertex twinOrigin, Face incidentFace) {
		this.origin = origin;
		this.twin = new HalfEdge(twinOrigin);
		this.incidentFace = incidentFace;

		origin.setIncidentEdge(this);
		this.twin.setTwin(this);
	}

	public HalfEdge(Vertex origin, Vertex twinOrigin) {
		this.origin = origin;
		this.twin = new HalfEdge(twinOrigin);

		origin.setIncidentEdge(this);
		this.twin.setTwin(this);
	}

	public boolean isSame(HalfEdge other) {

		if (other.getOrigin().getId() == this.origin.getId()
				&& other.getTwin().getOrigin().getId() == this.twin.getOrigin()
						.getId()) {
			return true;
		}

		return false;
	}

	// region Getters and Setters

	/**
	 * @return the origin
	 */
	public Vertex getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(Vertex origin) {
		this.origin = origin;
	}

	/**
	 * @return the twin
	 */
	public HalfEdge getTwin() {
		return twin;
	}

	/**
	 * @param twin
	 *            the twin to set
	 */
	public void setTwin(HalfEdge twin) {
		this.twin = twin;
	}

	/**
	 * @return the next
	 */
	public HalfEdge getNext() {
		return next;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(HalfEdge next) {
		this.next = next;
	}

	/**
	 * @return the previous
	 */
	public HalfEdge getPrevious() {
		return previous;
	}

	/**
	 * @param previous
	 *            the previous to set
	 */
	public void setPrevious(HalfEdge previous) {
		this.previous = previous;
	}

	/**
	 * @return the incidentFace
	 */
	public Face getIncidentFace() {
		return incidentFace;
	}

	/**
	 * @param incidentFace
	 *            the incidentFace to set
	 */
	public void setIncidentFace(Face incidentFace) {
		this.incidentFace = incidentFace;
	}

	// endregion
}
