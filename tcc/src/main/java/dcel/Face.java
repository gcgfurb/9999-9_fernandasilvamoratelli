package dcel;

import java.util.ArrayList;

public class Face {

	// Arbitrary half-edge bounding the face
	private HalfEdge boundary;

	public void addHalfEdge(HalfEdge he) {
		if (boundary == null) {
			boundary = he;
		} else {

			HalfEdge aux = boundary;

			while (aux.getNext() != null) {
				aux = aux.getNext();
			}

			aux.setNext(he);
			he.setPrevious(aux);

			HalfEdge auxTwin = boundary.getTwin();

			while (auxTwin.getPrevious() != null) {
				auxTwin = auxTwin.getPrevious();
			}

			auxTwin.setPrevious(he.getTwin());
			he.getTwin().setNext(auxTwin);
		}
	}
	
	public ArrayList<Face> getAdjacentFaces(){
		
		ArrayList<Face> adjacentFaces = new ArrayList<Face>();
		
		HalfEdge he = this.boundary;
		adjacentFaces.add(he.getTwin().getIncidentFace());
		
		while (he.getNext() != null) {
			he = he.getNext();
			
			adjacentFaces.add(he.getTwin().getIncidentFace());			
		}
		
		return adjacentFaces;
	}

	/**
	 * @return the boundary
	 */
	public HalfEdge getBoundary() {
		return boundary;
	}

	/**
	 * @param boundary
	 *            the boundary to set
	 */
	public void setBoundary(HalfEdge boundary) {
		this.boundary = boundary;
	}
}
