package dcel;

import parsing.ObjBag;
import parsing.ObjFace;
import parsing.ObjVertex;

public class PolyhedronBuilder {

	public Polyhedron Build(ObjBag objBag) {

		Polyhedron poly = new Polyhedron();

		Vertex vOrigin = null;
		Vertex vOriginTwin = null;

		Face fAux = null;

		ObjVertex[] vertices = objBag.getVertices();

		for (int i = 0; i < vertices.length; i++) {
			poly.addVertex(new Vertex(i + 1, vertices[i].x, vertices[i].y,
					vertices[i].z));
		}

		for (ObjFace face : objBag.getFaces()) {

			int[] faceVertices = face.getVert();
			fAux = new Face();

			for (int i = 0; i < faceVertices.length; i++) {

				vOrigin = poly.getVertexFromId(faceVertices[i] + 1);

				if (i + 1 < faceVertices.length) {
					vOriginTwin = poly.getVertexFromId(faceVertices[i + 1] + 1);
				} else {
					vOriginTwin = poly.getVertexFromId(faceVertices[0] + 1);
				}

				HalfEdge he = new HalfEdge(vOrigin, vOriginTwin, fAux);
				HalfEdge twin = he.getTwin();

				HalfEdge aux = poly.hasHalfEdge(he);
				if (aux == null) {
					poly.addHalfEdge(he);
				} else {
					if (aux.getIncidentFace() == null) {
						aux.setIncidentFace(fAux);
					}

					poly.addHalfEdge(aux);
				}

				aux = poly.hasHalfEdge(twin);
				if (aux == null) {
					poly.addHalfEdge(twin);
				} else {
					if (aux.getIncidentFace() == null) {
						aux.setIncidentFace(fAux);
					}

					poly.addHalfEdge(aux);
				}

				fAux.addHalfEdge(he);
			}

			poly.addFace(fAux);
		}

		for (Face face : poly.getFacesHE()) {
			HalfEdge he = face.getBoundary();
			HalfEdge twin = he.getTwin();

			System.out.println("Anticlockwise: **************************");
			System.out.println("v" + he.getOrigin().getId());
			while (he.getNext() != null) {
				System.out.println("v" + he.getNext().getOrigin().getId());

				he = he.getNext();
			}

			System.out.println("Clockwise: *******************************");
			System.out.println("v" + twin.getOrigin().getId());
			while (twin.getNext() != null) {
				System.out.println("v" + twin.getNext().getOrigin().getId());

				twin = twin.getNext();
			}

			face.getAdjacentFaces();
		}

		return poly;
	}
}
