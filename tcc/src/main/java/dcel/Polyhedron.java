package dcel;

import java.util.ArrayList;

public class Polyhedron {

	ArrayList<Vertex> verticesHE = new ArrayList<Vertex>();
	ArrayList<HalfEdge> halfedgesHE = new ArrayList<HalfEdge>();
	ArrayList<Face> facesHE = new ArrayList<Face>();

	public void addVertex(Vertex v) {
		verticesHE.add(v);
	}

	public boolean hasVertex(Vertex v) {
		for (Vertex vertex : verticesHE) {
			if (v.isSame(vertex)) {
				return true;
			}
		}

		return false;
	}

	public void addHalfEdge(HalfEdge he) {
		halfedgesHE.add(he);
	}

	public HalfEdge hasHalfEdge(HalfEdge he) {
		for (HalfEdge halfEdge : halfedgesHE) {
			if (he.isSame(halfEdge)) {
				return halfEdge;
			}
		}

		return null;
	}

	public void addFace(Face f) {
		facesHE.add(f);
	}

	public Vertex getVertexFromId(int id) {
		for (Vertex vertex : verticesHE) {
			if (vertex.getId() == id) {
				return vertex;
			}
		}

		return null;
	}

	// region Getters and Setters
	/**
	 * @return the verticesHE
	 */
	public ArrayList<Vertex> getVerticesHE() {
		return verticesHE;
	}

	/**
	 * @param verticesHE
	 *            the verticesHE to set
	 */
	public void setVerticesHE(ArrayList<Vertex> verticesHE) {
		this.verticesHE = verticesHE;
	}

	/**
	 * @return the halfedgesHE
	 */
	public ArrayList<HalfEdge> getHalfedgesHE() {
		return halfedgesHE;
	}

	/**
	 * @param halfedgesHE
	 *            the halfedgesHE to set
	 */
	public void setHalfedgesHE(ArrayList<HalfEdge> halfedgesHE) {
		this.halfedgesHE = halfedgesHE;
	}

	/**
	 * @return the facesHE
	 */
	public ArrayList<Face> getFacesHE() {
		return facesHE;
	}

	/**
	 * @param facesHE
	 *            the facesHE to set
	 */
	public void setFacesHE(ArrayList<Face> facesHE) {
		this.facesHE = facesHE;
	}

	// endregion
}
