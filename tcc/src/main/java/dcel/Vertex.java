package dcel;

public class Vertex {

	private int id;

	// Arbitrary half-edge that has v as its origin
	private HalfEdge incidentEdge;

	// Coordinates
	private double x;
	private double y;
	private double z;
	private double w;

	public Vertex(int id, double x, double y, double z) {

		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 1;
	}

	public Vertex(double x, double y, double z) {

		this.id = 0;
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 1;
	}

	public boolean isSame(Vertex other) {
		if (this.id == other.getId()) {
			return true;
		} else if (this.x == other.getX() && this.y == other.getY()
				&& this.z == other.getZ()) {
			return true;
		}

		return false;
	}

	// region Getters and Setters

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the incidentEdge
	 */
	public HalfEdge getIncidentEdge() {
		return incidentEdge;
	}

	/**
	 * @param incidentEdge
	 *            the incidentEdge to set
	 */
	public void setIncidentEdge(HalfEdge incidentEdge) {
		this.incidentEdge = incidentEdge;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * @return the z
	 */
	public double getZ() {
		return z;
	}

	/**
	 * @param z
	 *            the z to set
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * @return the w
	 */
	public double getW() {
		return w;
	}

	/**
	 * @param w
	 *            the w to set
	 */
	public void setW(double w) {
		this.w = w;
	}
	// endregion
}
