package gui;

import java.awt.Image;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.jogamp.newt.Window;

public class Scene {

	public static void main(String[] args) {
		// setup OpenGL Version 2
		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		// The canvas is the widget that's drawn in the JFrame
		GLCanvas glcanvas = new GLCanvas(capabilities);
		Renderer renderer = new Renderer();
		glcanvas.addGLEventListener(renderer);
		glcanvas.addKeyListener(renderer);
		glcanvas.setSize(500, 500);		

		JFrame frame = new JFrame("Polyedros");
		frame.getContentPane().add(glcanvas);

		// shutdown the program on windows close event
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(0);
			}
		});

		frame.setSize(frame.getContentPane().getPreferredSize());

		// UIManager.put("nimbusBase", new Color(25, 125, 125));
		// UIManager.put("nimbusBlueGrey", new Color(0));
		// UIManager.put("control", new Color(255,255,255));
		// UIManager.put("Button.background", new Color(0,0,255));

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// // If Nimbus is not available, you can set the GUI to another
			// look and feel.
		}

		JButton btnOpenFile = new JButton();
		JButton btnCreateNet = new JButton();

		try {
			// Open File
			Image imgOpenFile = ImageIO.read(Scene.class
					.getResource("/open obj_64.png"));
			Image newImgOpenFile = imgOpenFile.getScaledInstance(64, 64,
					java.awt.Image.SCALE_SMOOTH);
			btnOpenFile.setSize(64, 64);
			btnOpenFile.setToolTipText("Open File");

			btnOpenFile.setIcon(new ImageIcon(newImgOpenFile));

			// Create Net
			Image imgCreateNet = ImageIO.read(Scene.class
					.getResource("/open box_64.png"));
			Image newImgCreateNet = imgCreateNet.getScaledInstance(64, 64,
					java.awt.Image.SCALE_SMOOTH);
			btnCreateNet.setSize(64, 64);
			btnCreateNet.setToolTipText("Create Net");

			btnCreateNet.setIcon(new ImageIcon(newImgCreateNet));

			// Frame Icons
			Image imgApp16 = ImageIO.read(Scene.class.getResource("/16.png"));
			Image imgApp32 = ImageIO.read(Scene.class.getResource("/32.png"));
			Image imgApp64 = ImageIO.read(Scene.class.getResource("/64.png"));
			Image imgApp128 = ImageIO.read(Scene.class.getResource("/128.png"));

			List<Image> icons = new ArrayList<Image>();
			icons.add(imgApp16);
			icons.add(imgApp32);
			icons.add(imgApp64);
			icons.add(imgApp128);

			frame.setIconImages(icons);
		} catch (IOException ex) {
			btnOpenFile.setText("Open File");
			btnCreateNet.setText("Create Net");
		}

		JMenuBar mnuBar = new JMenuBar();
		mnuBar.add(btnOpenFile);
		mnuBar.add(btnCreateNet);

		frame.setJMenuBar(mnuBar);

		frame.setVisible(true);
	}
}
