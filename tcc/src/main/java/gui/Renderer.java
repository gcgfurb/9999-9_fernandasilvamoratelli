package gui;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import geometry.Edge;
import geometry.Face;
import geometry.Functions;
import geometry.Polyhedra;
import geometry.Transform;
import geometry.Vertex;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import com.jogamp.newt.event.KeyEvent;

import dcel.PolyhedronBuilder;

import java.awt.event.KeyListener;
import java.io.FileNotFoundException;

import parsing.ObjBag;
import parsing.ObjParser;
import planification.RotateCommand;
import bfs.Color;
import bfs.Graph;
import bfs.GraphNode;

public class Renderer implements GLEventListener, KeyListener {

	private GLU glu = new GLU();

	// angle of rotation for the camera direction
	float angleCamera = 0.0f;
	// actual vector representing the camera's direction
	double lx = 0.0f, lz = -1.0f;
	// XZ position of the camera
	float x = 0.0f, z = 5.0f;

	public void display(GLAutoDrawable gLDrawable) {
		final GL2 gl = gLDrawable.getGL().getGL2();
		GLU glu = new GLU(); // needed for lookat

		// Clear the drawing area
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		// Enable the OpenGL Blending functionality
		gl.glEnable(GL2.GL_BLEND);
		// Set the blend mode to blend our current RGBA with what is already in
		// the buffer
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

		// Reset the current matrix to the "identity"
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		// glu.gluLookAt(x, 1.0f, z, x + lx, 1.0f, z + lz, 0.0f, 1.0f, 0.0f);

		glu.gluLookAt(5, 5, 5, // eye pos
				0, 0, 0, // look at
				0, 0, 1); // positive z vector
		// gl.glRotatef(angleCamera, 0.f, 1.f, 0.f);/* orbit the Y axis */

		// gl.glRotatef(290.0f, 0.0f, 0.0f, 1.0f);
		// gl.glTranslatef(0.0f, 0.0f, -5.0f);

		Polyhedra poly = generatePoly(gl);

		drawAxis(gl);
	}

	private void drawAxis(GL2 gl) {
		gl.glPushMatrix();

		gl.glColorMaterial(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE);

		gl.glLineWidth(2.5f);

		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(100, 0, 0);
		gl.glEnd();

		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, 100, 0);
		gl.glEnd();

		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL2.GL_LINES);
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(0, 0, 100);
		gl.glEnd();

		gl.glPopMatrix();

		gl.glFlush();
	}

	public void displayChanged(GLAutoDrawable gLDrawable, boolean modeChanged,
			boolean deviceChanged) {
		System.out.println("displayChanged called");
	}

	public void init(GLAutoDrawable gLDrawable) {
		System.out.println("init() called");
		GL2 gl = gLDrawable.getGL().getGL2();

		// Set background color and shading mode
		gl.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
		gl.glShadeModel(GL2.GL_COLOR_BUFFER_BIT);

		// Set light
		float ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float position[] = { -20.0f, -30.0f, 20.0f, 0.0f };
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffuse, 0);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);

		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
	}

	public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width,
			int height) {
		System.out.println("reshape() called: x = " + x + ", y = " + y
				+ ", width = " + width + ", height = " + height);
		final GL2 gl = gLDrawable.getGL().getGL2();

		if (height <= 0) // avoid a divide by zero error!
		{
			height = 1;
		}

		final float h = (float) width / (float) height;

		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, h, 1.0, 20.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	public void dispose(GLAutoDrawable arg0) {
		System.out.println("dispose() called");
	}

	private Polyhedra generatePoly(GL2 gl) {

		try {
			ObjBag bag = ObjParser.CarregaObjeto();
			PolyhedronBuilder boo = new PolyhedronBuilder();
			boo.Build(bag);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Polyhedra poly = ObjParser.getCube();
		// Graph graph = ObjParser.getCubeGraph();

		Polyhedra poly = ObjParser.getTetrahedron();
		Graph graph = ObjParser.getTetrahedronGraph();

		generateBFS(graph, gl);

		return poly;
	}

	private double getAngle(Vertex normalVector1, Vertex normalVector2) {

		normalVector1 = normalVector1.normalize();
		normalVector2 = normalVector2.normalize();

		double product = Functions.dotProduct(normalVector1, normalVector2);
		double magNormalV = Math.sqrt(Functions.dotProduct(normalVector1,
				normalVector1));
		double magPlaneNV = Math.sqrt(Functions.dotProduct(normalVector2,
				normalVector2));

		return Math.acos(product / (magNormalV * magPlaneNV));
	}

	private void generateBFS(Graph graph, GL2 gl) {

		BFS(graph, gl);

		for (GraphNode gn : graph.getNodes()) {

			System.out.println(" -------- ");
			System.out.println(" NODE " + gn.getFace().getDescription());

			System.out.println("Distance =" + gn.getDistance());
			System.out.println("Color =" + gn.getColor());

			if (gn.getPredecessor() == null) {
				System.out.println("Predecessor = NULL");
			} else {
				System.out.println("Predecessor ="
						+ gn.getPredecessor().getFace().getDescription());
			}
		}
	}

	public void BFS(Graph graph, GL2 gl) {

		Polyhedra planarNet = new Polyhedra();

		Queue<GraphNode> queue = new LinkedList<GraphNode>();

		Set<GraphNode> nodes = graph.getNodes();

		if (nodes != null && nodes.size() > 0) {

			GraphNode s = nodes.iterator().next();
			nodes.remove(s);

			for (GraphNode gn : nodes) {

				gn.setColor(Color.WHITE);
				gn.setDistance(Integer.MAX_VALUE);
				gn.setPredecessor(null);
			}

			s.setColor(Color.GRAY);
			s.setDistance(0);
			s.setPredecessor(null);

			queue.add(s);

			while (queue.size() > 0) {
				GraphNode u = queue.poll();

				// drawFace(gl, u);

				for (GraphNode adj : u.getAdjacencyList()) {

					if (adj.getColor() == Color.WHITE) {
						adj.setColor(Color.GRAY);
						adj.setDistance(u.getDistance() + 1);
						adj.setPredecessor(u);

						queue.add(adj);
						// processFace(planarNet, adj);
					}
				}

				u.setColor(Color.BLACK);

				if (u.getPredecessor() != null) {
					// u.setPredecessorEdgeId(u.getPredecessor().getFace()
					// .getEdgeInCommon(u.getFace()).getId());
				}

				processFace(planarNet, u);
			}

			for (Face f : planarNet.getFaces()) {
				// gl.glPushMatrix();
				// gl.glMultMatrixd(transform.GetData(), 0);
				gl.glBegin(GL2.GL_POLYGON);
				gl.glColorMaterial(GL2.GL_FRONT_AND_BACK,
						GL2.GL_AMBIENT_AND_DIFFUSE);

				switch (f.getFaceID()) {
				case 1: // Front - Red
					gl.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
					break;
				case 2: // Back - Green
					gl.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
					break;
				case 3: // Right - Purple
					gl.glColor4f(0.5f, 0.0f, 1.0f, 1.0f);
					break;
				case 4: // Top - Orange
					gl.glColor4f(1.0f, 0.5f, 0.0f, 1.0f);
					break;
				case 5: // Left - Yellow
					gl.glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
					break;
				case 6: // Bottom - Blue
					gl.glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
					break;
				default:
					gl.glColor3f(1.0f, 1.0f, 1.0f);
					break;
				}

				System.out.println("FACE: " + f.getFaceID());

				for (Vertex v : f.getVertices()) {
					gl.glVertex3d(v.getX(), v.getY(), v.getZ());
					System.out.println(v.getX() + ", " + v.getY() + ", "
							+ v.getZ());
				}

				gl.glEnd();

				gl.glLineWidth(1.0f);
				gl.glBegin(GL2.GL_LINES);
				gl.glColorMaterial(GL2.GL_FRONT_AND_BACK,
						GL2.GL_AMBIENT_AND_DIFFUSE);
				gl.glColor3f(0.0f, 0.0f, 0.0f);

				for (Vertex v : f.getVertices()) {
					gl.glVertex3d(v.getX(), v.getY(), v.getZ());
				}

				gl.glEnd();

				// gl.glPopMatrix();

				System.out.println();
			}

			gl.glFlush();
		}
	}

	private void processFace(Polyhedra planarNet, GraphNode u) {
		Face f = u.getFace();
		Vertex axis = new Vertex();
		double angle = 0;

		Transform transform = new Transform();

		if (u.getPredecessor() == null) {

			System.out.println("Processing root FACE: " + f.getFaceID());
		} else {

			System.out.println("Processing FACE: " + f.getFaceID());
			System.out.println("Predecessor: "
					+ u.getPredecessor().getFace().getFaceID());

			Face aux = u.getPredecessor().getFace();
			Face newFace = planarNet.getFaceFromId(aux.getFaceID());

			axis = Functions.crossProduct(f.getNormalVector(),
					u.getPredecessor().getFace().getNormalVector()).normalize();

			// axis = Functions.crossProduct(f.calculateFaceNormal(),
			// newFace.calculateFaceNormal());

			axis = Functions.crossProduct(f.calculateFaceNormal(),
					newFace.calculateFaceNormal()).normalize();

			// angle = getAngleToPlane(f.getNormalVector());
			angle = Math.PI
					- getAngle(f.getNormalVector(), u.getPredecessor()
							.getFace().getNormalVector());

			Edge teste = f.getEdgeInCommon(aux);
			System.out.println("TESTE:");
			Vertex lalala = new Vertex(teste.getSource().getX()
					- teste.getTarget().getX(), teste.getSource().getY()
					- teste.getTarget().getY(), teste.getSource().getZ()
					- teste.getTarget().getZ()).normalize();

			System.out.println(lalala.getX() + ", " + lalala.getY() + ", "
					+ lalala.getZ());

			// Vertex closest = f.getClosestToOrigin().clone();
			Vertex closest = f.getVertexInCommon(u.getPredecessor().getFace())
					.clone();
			transform.TranslateTo(new Vertex(-closest.getX(), -closest.getY(),
					-closest.getZ()), f);

			switch (f.getFaceID()) {
			case 1: // Front - Red
				// transform.RotateAroundAxis(new Vertex(0, 0, 0), 0.0, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {
					transform.RotateAroundAxis(lalala, -angle, f);
					// transform.RotateAroundAxis(axis, -angle, f);

					// u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			case 2: // Back - Green
				// transform.RotateAroundAxis(new Vertex(0, 1, 0),
				// 1.5707963267948966, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {

					transform.RotateAroundAxis(lalala, -angle, f);
					// transform.RotateAroundAxis(axis, -angle, f);

					// u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			case 3: // Right - Purple
				// transform.RotateAroundAxis(new Vertex(0, 0, -1),
				// -1.5707963267948966, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {

					transform.RotateAroundAxis(lalala, -angle, f);
					// transform.RotateAroundAxis(axis, -angle, f);

					// u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			case 4: // Top - Orange
				// transform.RotateAroundAxis(new Vertex(1, 0, 0),
				// -1.5707963267948966, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {

					transform.RotateAroundAxis(lalala, -angle, f);
					// transform.RotateAroundAxis(axis, -angle, f);

					// u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			case 5: // Left - Yellow
				// transform.RotateAroundAxis(new Vertex(0, 0, 1),
				// -1.5707963267948966, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {
					transform.RotateAroundAxis(axis, -angle, f);

					u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			case 6: // Bottom - Blue
				// transform.RotateAroundAxis(new Vertex(1, 0, 0),
				// -1.5707963267948966, f);

				if (angle != 0 && angle != Math.PI && !axis.isZero()) {
					transform.RotateAroundAxis(axis, -angle, f);

					u.setRotation(new RotateCommand(angle, axis, f));
				}
				break;
			default:
				// transform.RotateAroundAxis(new Vertex(0, 1, 0),
				// 1.5707963267948966, f);
				break;
			}

			transform.TranslateTo(closest, f);

			// if (u.getPredecessor().getRotation() != null) {
			// RotateCommand r = (RotateCommand) u.getPredecessor()
			// .getRotation();
			//
			// closest = f.getVertexInCommon(u.getPredecessor().getFace())
			// .clone();
			// transform.TranslateTo(
			// new Vertex(-closest.getX(), -closest.getY(), -closest
			// .getZ()), f);
			//
			// transform.RotateAroundAxis(r.getAxis(), -r.getAngle(), f);
			//
			// Vertex v = newFace.getVertexFromID(closest.getVertexID())
			// .clone();
			// transform.TranslateTo(new Vertex(v.getX(), v.getY(), v.getZ()),
			// f);
			// }

			// System.out.println("Predecessor p/ trans: " +
			// newFace.getFaceID());

			// Vertex v1 = f.getVertexInCommon(aux);
			// Vertex v2 = newFace.getVertexFromID(v1.getVertexID());

			// if (!v2.equals(f.getVertexFromID(v2.getVertexID()))) {
			// transform.TranslateTo(
			// new Vertex(-v2.getX(), -v2.getY(), -v2.getZ()), f);
			// }
		}

		planarNet.addFace(f);
	}

	private void drawFace(GL2 gl, GraphNode u) {

		Face f = u.getFace();
		Transform matrixGlobal = new Transform();

		System.out.println("FACE: " + f.getFaceID());

		for (Vertex v : f.getVertices()) {
			System.out.println(v.getX() + ", " + v.getY() + ", " + v.getZ());
		}

		Vertex axis = new Vertex();
		double angle = 0;
		Vertex edgeV = null;
		if (u.getPredecessor() == null) {

			Vertex planeNormal = new Vertex(-1.0f, 0.0f, 0.0f);
			axis = Functions.crossProduct(f.getNormalVector(), planeNormal);
			angle = 0;// getAngleToPlane(f.getNormalVector());
			// translate(f, matrixGlobal, new Vertex(-axis.getX(), -axis.getY(),
			// -axis.getZ()));

			// rotate(f, matrixGlobal, getAngleToPlane(f.getNormalVector()),
			// planeNormal);

			// translate(f, matrixGlobal, new Vertex(axis.getX(), axis.getY(),
			// axis.getZ()));
		} else {

			System.out.println("Precessor: "
					+ u.getPredecessor().getFace().getFaceID());
			Vertex common = u.getPredecessor().getFace().getVertexInCommon(f);

			axis = Functions.crossProduct(f.getNormalVector(), u
					.getPredecessor().getFace().getNormalVector());
			Edge edge = u.getPredecessor().getFace().getEdgeInCommon(f);
			edgeV = new Vertex(edge.getTarget().getX()
					- edge.getSource().getX(), edge.getTarget().getY()
					- edge.getSource().getY(), edge.getTarget().getZ()
					- edge.getSource().getZ());
			angle = getAngle(f.getNormalVector(), u.getPredecessor().getFace()
					.getNormalVector());
			// translate(f, matrixGlobal, new Vertex(-axis.getX(), -axis.getY(),
			// -axis.getZ()));

			// rotate(f,
			// matrixGlobal,
			// getAngle(f.getNormalVector(), u.getPredecessor().getFace()
			// .getNormalVector()), u.getPredecessor().getFace()
			// .getNormalVector());

			// translate(f, matrixGlobal, new Vertex(axis.getX(), axis.getY(),
			// axis.getZ()));
		}

		System.out.println("Angle: " + Math.toDegrees(angle));
		System.out.println("Axis: {" + axis.getX() + ", " + axis.getY() + ", "
				+ axis.getZ() + "}");

		gl.glPushMatrix();
		// gl.glMultMatrixd(f.getTransform().GetData(), 0);

		double angleD = Math.toDegrees(angle);// + 180) % 360;

		gl.glTranslated(axis.getX(), axis.getY(), axis.getZ());

		// if (edgeV != null) {
		// gl.glTranslated(-edgeV.getX(), -edgeV.getY(), -edgeV.getZ());
		// gl.glTranslated(edgeV.getX(), edgeV.getY(), edgeV.getZ());
		// }

		// gl.glTranslated(-axis.getX(), -axis.getY(), -axis.getZ());
		gl.glRotated(angleD, axis.getX(), axis.getY(), axis.getZ());
		// gl.glTranslated(axis.getX(), axis.getY(), axis.getZ());

		gl.glBegin(GL2.GL_POLYGON);

		gl.glNormal3d(f.getNormalVector().getX(), f.getNormalVector().getY(), f
				.getNormalVector().getZ());

		for (Vertex v : f.getVertices()) {

			gl.glColorMaterial(GL2.GL_FRONT_AND_BACK,
					GL2.GL_AMBIENT_AND_DIFFUSE);
			gl.glColor3f(1.0f, 0.5f, 0.0f);
			gl.glVertex3d(v.getX(), v.getY(), v.getZ());
		}

		gl.glEnd();
		gl.glPopMatrix();

		System.out.println();

		gl.glFlush();
	}

	@Override
	public void keyPressed(java.awt.event.KeyEvent e) {

		float fraction = 0.1f;

		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			angleCamera -= 0.01f;
			lx = Math.sin(angleCamera);
			lz = -Math.cos(angleCamera);
		}

		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			angleCamera += 0.01f;
			lx = Math.sin(angleCamera);
			lz = -Math.cos(angleCamera);
		}

		if (e.getKeyCode() == KeyEvent.VK_UP) {
			x += lx * fraction;
			z += lz * fraction;
		}

		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			x -= lx * fraction;
			z -= lz * fraction;
		}
	}

	@Override
	public void keyReleased(java.awt.event.KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(java.awt.event.KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
