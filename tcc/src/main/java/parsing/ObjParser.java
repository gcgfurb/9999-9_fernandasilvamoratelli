package parsing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Scanner;

import bfs.Graph;
import bfs.GraphEdge;
import bfs.GraphNode;
import geometry.Face;
import geometry.Polyhedra;
import geometry.Vertex;

public class ObjParser {

	public static Polyhedra getCube() {
		// v1 1.0 -1.0 -1.0
		Vertex v1 = new Vertex(1, 1.0f, -1.0f, -1.0f);
		// v2 1.0 -1.0 1.0
		Vertex v2 = new Vertex(2, 1.0f, -1.0f, 1.0f);
		// v3 -1.0 -1.0 1.0
		Vertex v3 = new Vertex(3, -1.0f, -1.0f, 1.0f);
		// v4 -1.0 -1.0 -1.0
		Vertex v4 = new Vertex(4, -1.0f, -1.0f, -1.0f);
		// v5 1.0 1.0 -1.0
		Vertex v5 = new Vertex(5, 1.0f, 1.0f, -1.0f);
		// v6 1.0 1.0 1.0
		Vertex v6 = new Vertex(6, 1.0f, 1.0f, 1.0f);
		// v7 -1.0 1.0 1.0
		Vertex v7 = new Vertex(7, -1.0f, 1.0f, 1.0f);
		// v8 -1.0 1.0 -1.0
		Vertex v8 = new Vertex(8, -1.0f, 1.0f, -1.0f);

		// Front = 1 2 3 4
		Face front = new Face(1);
		front.setDescription("front - red");
		front.setNormalVector(new Vertex(0.0f, -1.0f, 0.0f));
		front.addVertex(v1);
		front.addVertex(v2);
		front.addVertex(v3);
		front.addVertex(v4);
		// Back = 5 8 7 6
		Face back = new Face(2);
		back.setDescription("back - green");
		back.setNormalVector(new Vertex(0.0f, 1.0f, 0.0f));
		back.addVertex(v5);
		back.addVertex(v8);
		back.addVertex(v7);
		back.addVertex(v6);
		// Right = 1 5 6 2
		Face right = new Face(3);
		right.setDescription("right - purple");
		right.setNormalVector(new Vertex(1.0f, 0.0f, 0.0f));
		right.addVertex(v1);
		right.addVertex(v5);
		right.addVertex(v6);
		right.addVertex(v2);
		// Top = 2 6 7 3
		Face top = new Face(4);
		top.setDescription("top - orange");
		top.setNormalVector(new Vertex(0.0f, 0.0f, 1.0f));
		top.addVertex(v2);
		top.addVertex(v6);
		top.addVertex(v7);
		top.addVertex(v3);
		// Left = 3 7 8 4
		Face left = new Face(5);
		left.setDescription("left - yellow");
		left.setNormalVector(new Vertex(-1.0f, 0.0f, 0.0f));
		left.addVertex(v3);
		left.addVertex(v7);
		left.addVertex(v8);
		left.addVertex(v4);
		// Bottom = 5 1 4 8
		Face bottom = new Face(6);
		bottom.setDescription("bottom - blue");
		bottom.setNormalVector(new Vertex(0.0f, 0.0f, -1.0f));
		bottom.addVertex(v5);
		bottom.addVertex(v1);
		bottom.addVertex(v4);
		bottom.addVertex(v8);

		LinkedHashSet<Face> faces = new LinkedHashSet<>();
		faces.add(front);
		faces.add(top);
		faces.add(right);
		faces.add(bottom);
		faces.add(back);
		faces.add(left);

		Polyhedra poly = new Polyhedra(faces);

		return poly;
	}

	public static Graph getCubeGraph() {
		// v1 1.0 -1.0 -1.0
		Vertex v1 = new Vertex(1, 1.0f, -1.0f, -1.0f);
		// v2 1.0 -1.0 1.0
		Vertex v2 = new Vertex(2, 1.0f, -1.0f, 1.0f);
		// v3 -1.0 -1.0 1.0
		Vertex v3 = new Vertex(3, -1.0f, -1.0f, 1.0f);
		// v4 -1.0 -1.0 -1.0
		Vertex v4 = new Vertex(4, -1.0f, -1.0f, -1.0f);
		// v5 1.0 1.0 -1.0
		Vertex v5 = new Vertex(5, 1.0f, 1.0f, -1.0f);
		// v6 1.0 1.0 1.0
		Vertex v6 = new Vertex(6, 1.0f, 1.0f, 1.0f);
		// v7 -1.0 1.0 1.0
		Vertex v7 = new Vertex(7, -1.0f, 1.0f, 1.0f);
		// v8 -1.0 1.0 -1.0
		Vertex v8 = new Vertex(8, -1.0f, 1.0f, -1.0f);

		// Front = 1 2 3 4
		Face front = new Face(1);
		front.setDescription("front - red");
		front.setNormalVector(new Vertex(0.0f, -1.0f, 0.0f));
		front.addVertex(v1);
		front.addVertex(v2);
		front.addVertex(v3);
		front.addVertex(v4);
		// Back = 5 8 7 6
		Face back = new Face(2);
		back.setDescription("back - green");
		back.setNormalVector(new Vertex(0.0f, 1.0f, 0.0f));
		back.addVertex(v5);
		back.addVertex(v8);
		back.addVertex(v7);
		back.addVertex(v6);
		// Right = 1 5 6 2
		Face right = new Face(3);
		right.setDescription("right - purple");
		right.setNormalVector(new Vertex(1.0f, 0.0f, 0.0f));
		right.addVertex(v1);
		right.addVertex(v5);
		right.addVertex(v6);
		right.addVertex(v2);
		// Top = 2 6 7 3
		Face top = new Face(4);
		top.setDescription("top - orange");
		top.setNormalVector(new Vertex(0.0f, 0.0f, 1.0f));
		top.addVertex(v2);
		top.addVertex(v6);
		top.addVertex(v7);
		top.addVertex(v3);
		// Left = 3 7 8 4
		Face left = new Face(5);
		left.setDescription("left - yellow");
		left.setNormalVector(new Vertex(-1.0f, 0.0f, 0.0f));
		left.addVertex(v3);
		left.addVertex(v7);
		left.addVertex(v8);
		left.addVertex(v4);
		// Bottom = 5 1 4 8
		Face bottom = new Face(6);
		bottom.setDescription("bottom - blue");
		bottom.setNormalVector(new Vertex(0.0f, 0.0f, -1.0f));
		bottom.addVertex(v5);
		bottom.addVertex(v1);
		bottom.addVertex(v4);
		bottom.addVertex(v8);

		GraphNode nFront = new GraphNode(front);
		GraphNode nBack = new GraphNode(back);
		GraphNode nTop = new GraphNode(top);
		GraphNode nBottom = new GraphNode(bottom);
		GraphNode nLeft = new GraphNode(left);
		GraphNode nRight = new GraphNode(right);

		GraphEdge eFrontTop = new GraphEdge(nFront, nTop);
		GraphEdge eFrontBottom = new GraphEdge(nFront, nBottom);
		GraphEdge eFrontLeft = new GraphEdge(nFront, nLeft);
		GraphEdge eFrontRight = new GraphEdge(nFront, nRight);
		GraphEdge eBackTop = new GraphEdge(nBack, nTop);
		GraphEdge eBackBottom = new GraphEdge(nBack, nBottom);
		GraphEdge eBackLeft = new GraphEdge(nBack, nLeft);
		GraphEdge eBackRight = new GraphEdge(nBack, nRight);
		GraphEdge eTopLeft = new GraphEdge(nTop, nLeft);
		GraphEdge eTopRight = new GraphEdge(nTop, nRight);
		GraphEdge eBottomLeft = new GraphEdge(nBottom, nLeft);
		GraphEdge eBottomRight = new GraphEdge(nBottom, nRight);

		Graph graph = new Graph();

		graph.addNode(nFront);
		graph.addNode(nBack);
		graph.addNode(nTop);
		graph.addNode(nBottom);
		graph.addNode(nLeft);
		graph.addNode(nRight);

		graph.addEdge(eFrontTop);
		graph.addEdge(eFrontBottom);
		graph.addEdge(eFrontLeft);
		graph.addEdge(eFrontRight);
		graph.addEdge(eBackTop);
		graph.addEdge(eBackLeft);
		graph.addEdge(eBackRight);
		graph.addEdge(eBackBottom);
		graph.addEdge(eTopLeft);
		graph.addEdge(eTopRight);
		graph.addEdge(eBottomLeft);
		graph.addEdge(eBottomRight);

		return graph;
	}

	public static Polyhedra getTetrahedron() {

		// v1 1.0000 -0.0000 1.0000
		Vertex v1 = new Vertex(1, 1.0000, -0.0000, 1.0000);
		// v2 0.0000 -0.0000 -1.0000
		Vertex v2 = new Vertex(2, 0.0000, -0.0000, -1.0000);
		// v3 0.0000 1.0000 -0.0000
		Vertex v3 = new Vertex(3, 0.0000, 1.0000, -0.0000);
		// v4 -1.0000 -0.0000 1.0000
		Vertex v4 = new Vertex(4, -1.0000, -0.0000, 1.0000);

		// vn1 0.8165 0.4082 -0.4082
		Vertex vn1 = new Vertex(0.8165, 0.4082, -0.4082);
		// vn2 -0.8165 0.4082 -0.4082
		Vertex vn2 = new Vertex(-0.8165, 0.4082, -0.4082);
		// vn3 0.0000 0.7071 0.7071
		Vertex vn3 = new Vertex(0.0000, 0.7071, 0.7071);
		// vn4 0.0000 -1.0000 -0.0000
		Vertex vn4 = new Vertex(0.0000, -1.0000, -0.0000);

		// f1 1/1/1 2/2/1 3/3/1
		Face f1 = new Face(1);
		f1.setDescription("f1 - red");
		f1.setNormalVector(vn1);
		f1.addVertex(v1);
		f1.addVertex(v2);
		f1.addVertex(v3);

		// f2 2/2/2 4/4/2 3/3/2
		Face f2 = new Face(2);
		f2.setDescription("f2 - green");
		f2.setNormalVector(vn2);
		f2.addVertex(v2);
		f2.addVertex(v4);
		f2.addVertex(v3);

		// f3 4/4/3 1/5/3 3/3/3
		Face f3 = new Face(3);
		f3.setDescription("f3 - purple");
		f3.setNormalVector(vn3);
		f3.addVertex(v4);
		f3.addVertex(v1);
		f3.addVertex(v3);

		// f4 2/4/4 1/5/4 4/3/4
		Face f4 = new Face(4);
		f4.setDescription("f4 - orange");
		f4.setNormalVector(vn4);
		f4.addVertex(v2);
		f4.addVertex(v1);
		f4.addVertex(v4);

		LinkedHashSet<Face> faces = new LinkedHashSet<>();
		faces.add(f1);
		faces.add(f2);
		faces.add(f3);
		faces.add(f4);

		Polyhedra poly = new Polyhedra(faces);

		return poly;
	}

	public static Graph getTetrahedronGraph() {
		// v1 1.0000 -0.0000 1.0000
		Vertex v1 = new Vertex(1, 1.0000, -0.0000, 1.0000);
		// v2 0.0000 -0.0000 -1.0000
		Vertex v2 = new Vertex(2, 0.0000, -0.0000, -1.0000);
		// v3 0.0000 1.0000 -0.0000
		Vertex v3 = new Vertex(3, 0.0000, 1.0000, -0.0000);
		// v4 -1.0000 -0.0000 1.0000
		Vertex v4 = new Vertex(4, -1.0000, -0.0000, 1.0000);

		// vn1 0.7746 0.4472 -0.4472
		Vertex vn1 = new Vertex(0.7746, 0.4472, -0.4472);
		// vn2 -0.7746 0.4472 -0.4472
		Vertex vn2 = new Vertex(-0.7746, 0.4472, -0.4472);
		// vn3 -0.0000 0.4472 0.8944
		Vertex vn3 = new Vertex(-0.0000, 0.4472, 0.8944);
		// vn4 0.0000 -1.0000 -0.0000
		Vertex vn4 = new Vertex(0.0000, -1.0000, -0.0000);

		// f1 1/1/1 2/2/1 3/3/1
		Face f1 = new Face(1);
		f1.setDescription("f1 - red");
		f1.setNormalVector(vn1);
		f1.addVertex(v1);
		f1.addVertex(v2);
		f1.addVertex(v3);

		// f2 2/2/2 4/4/2 3/3/2
		Face f2 = new Face(2);
		f2.setDescription("f2 - green");
		f2.setNormalVector(vn2);
		f2.addVertex(v2);
		f2.addVertex(v4);
		f2.addVertex(v3);

		// f3 4/4/3 1/5/3 3/3/3
		Face f3 = new Face(3);
		f3.setDescription("f3 - purple");
		f3.setNormalVector(vn3);
		f3.addVertex(v4);
		f3.addVertex(v1);
		f3.addVertex(v3);

		// f4 2/4/4 1/5/4 4/3/4
		Face f4 = new Face(4);
		f4.setDescription("f4 - orange");
		f4.setNormalVector(vn4);
		f4.addVertex(v2);
		f4.addVertex(v1);
		f4.addVertex(v4);

		LinkedHashSet<Face> faces = new LinkedHashSet<>();
		faces.add(f1);
		faces.add(f2);
		faces.add(f3);
		faces.add(f4);

		GraphNode nF1 = new GraphNode(f1);
		GraphNode nF2 = new GraphNode(f2);
		GraphNode nF3 = new GraphNode(f3);
		GraphNode nF4 = new GraphNode(f4);

		GraphEdge eF1F2 = new GraphEdge(nF1, nF2);
		GraphEdge eF1F3 = new GraphEdge(nF1, nF3);
		GraphEdge eF1F4 = new GraphEdge(nF1, nF4);
		GraphEdge eF2F3 = new GraphEdge(nF2, nF3);
		GraphEdge eF2F4 = new GraphEdge(nF2, nF4);
		GraphEdge eF3F4 = new GraphEdge(nF3, nF4);

		Graph graph = new Graph();

		graph.addNode(nF1);
		graph.addNode(nF2);
		graph.addNode(nF3);
		graph.addNode(nF4);

		graph.addEdge(eF1F2);
		graph.addEdge(eF1F3);
		graph.addEdge(eF1F4);
		graph.addEdge(eF2F3);
		graph.addEdge(eF2F4);
		graph.addEdge(eF3F4);

		return graph;
	}

	// Define a estrutura de uma coordenada
	// de textura (s,t,r) - r nao eh usado
	// typedef struct {
	// GLfloat s,t,r;
	// } TEXCOORD;

	// Define um material
	// typedef struct {
	// char nome[20]; // IdentificaÃ§Ã£o do material
	// GLfloat ka[4]; // Ambiente
	// GLfloat kd[4]; // Difuso
	// GLfloat ks[4]; // Especular
	// GLfloat ke[4]; // EmissÃ£o
	// GLfloat spec; // Fator de especularidade
	// } MAT;

	// Cria e carrega um objeto 3D que esteja armazenado em um
	// arquivo no formato OBJ, cujo nome eh passado por parametro.
	// eh feita a leitura do arquivo para preencher as estruturas
	// de vertices e faces, que sao retornadas atraves de um OBJ.
	//
	// O parametro mipmap indica se deve-se gerar mipmaps a partir
	// das texturas (se houver)
	// private ObjBag CarregaObjeto(String nomeArquivo, boolean mipmap)
	public static ObjBag CarregaObjeto() throws FileNotFoundException {
		int i;
		int vcont;
		int ncont;
		int fcont;
		int tcont;
		int material;
		int texid;
		String aux = new String(new char[256]);
		// TEX ptr;

		ObjBag obj = new ObjBag();

		File text = new File("C:/temp/cube.obj");
		Scanner scnr = new Scanner(text);

		// A primeira passagem serve apenas para contar quantos
		// elementos existem no arquivo - necessario para
		// alocar memoria depois
		int lineNumber = 1;
		while (scnr.hasNextLine()) {

			String line = scnr.nextLine();
			System.out.println("line " + lineNumber + " :" + line);
			lineNumber++;

			// Skip blank lines and comments
			if (line.length() == 0 || line.startsWith("#")) {
				continue;
			}

			Scanner lineScanner = new Scanner(line);

			// encontramos um vertice
			if (lineScanner.hasNext("v")) {
				obj.numVertices++;
			}

			// encontramos uma face
			if (lineScanner.hasNext("f")) {
				obj.numFaces++;
			}

			// encontramos uma normal
			if (lineScanner.hasNext("vn")) {
				obj.numNormais++;
			}

			// encontramos uma texcoord
			if (lineScanner.hasNext("vt")) {
				obj.numTexcoords++;
			}

			lineScanner.close();
		}

		scnr.close();

		// Agora voltamos ao inicio do arquivo para ler os elementos
		Scanner newScnr = new Scanner(text);

		// Aloca os vertices
		obj.vertices = new ObjVertex[obj.numVertices];

		for (int j = 0; j < obj.vertices.length; j++) {
			obj.vertices[j] = new ObjVertex();
		}

		// Aloca as faces
		obj.faces = new ObjFace[obj.numFaces];

		for (int j = 0; j < obj.faces.length; j++) {
			obj.faces[j] = new ObjFace();
		}

		// Aloca as normais
		obj.normais = new ObjVertex[obj.numNormais];

		for (int j = 0; j < obj.normais.length; j++) {
			obj.normais[j] = new ObjVertex();
		}

		// A segunda passagem e para ler efetivamente os
		// elementos do arquivo, ja que sabemos quantos
		// tem de cada tipo
		vcont = 0;
		ncont = 0;
		tcont = 0;
		fcont = 0;

		// Material corrente = nenhum
		material = -1;
		// Textura corrente = nenhuma
		texid = -1;

		while (newScnr.hasNextLine()) {

			String line = newScnr.nextLine();

			// Skip blank lines and comments
			if (line.length() == 0 || line.startsWith("#")) {
				continue;
			}

			Scanner lineScanner = new Scanner(line).useLocale(Locale.US);
			lineScanner.useDelimiter(" ");

			// Vertice ?
			if (lineScanner.hasNext("v")) {

				lineScanner.next(); // Skips v
				obj.vertices[vcont].x = lineScanner.nextFloat(); // x
				obj.vertices[vcont].y = lineScanner.nextFloat(); // y
				obj.vertices[vcont].z = lineScanner.nextFloat(); // y

				vcont++;
			}

			// Normal ?
			if (lineScanner.hasNext("vn")) {

				lineScanner.next(); // Skips vn
				obj.normais[ncont].x = lineScanner.nextFloat(); // x
				obj.normais[ncont].y = lineScanner.nextFloat(); // y
				obj.normais[ncont].z = lineScanner.nextFloat(); // y
				ncont++;

				// Registra que o arquivo possui definicao de normais
				// por vertice
				obj.normais_por_vertice = true;
			}

			// Face ?
			if (lineScanner.hasNext("f")) {

				// Skips f
				lineScanner.next();

				// Associa a  face o indice do material corrente, ou -1
				// se nao houver
				obj.faces[fcont].mat = material;

				// Associa a  face o texid da textura seleciona ou -1 se
				// nao houver
				obj.faces[fcont].texid = texid;

				// Temporarios para armazenar os indices desta face
				int[] vi = new int[10];
				int[] ti = new int[10];
				int[] ni = new int[10];

				obj.faces[fcont].nv = 0;
				int nv = 0;
				int vIndex = 0;
				boolean tem_t = false;
				boolean tem_n = false;

				// Interpreta a descricao da face
				while (lineScanner.hasNext()) {

					// LÃª Ã­ndice do vÃ©rtice
					// vi[nv] = leNum(ptr, sep);
					Scanner faceScanner = new Scanner(lineScanner.next())
							.useLocale(Locale.US);
					faceScanner.useDelimiter("/");
					String fAux = "";

					if (faceScanner.hasNext()) {

						fAux = faceScanner.next();

						vIndex = tryParseInt(fAux);
						if (vIndex != -1) {
							vi[nv] = vIndex;
						}
					} else {
						continue;
					}

					if (!faceScanner.hasNext()) {
						// Skip to the next vertex
						nv++;
						continue;
					}

					// Le indice da texcoord
					fAux = faceScanner.next();

					vIndex = tryParseInt(fAux);
					if (vIndex != -1) {
						ti[nv] = vIndex;
						tem_t = true;
					}

					if (!faceScanner.hasNext()) {
						// Skip to the next vertex
						nv++;
						continue;
					}

					// Normal index
					fAux = faceScanner.next();

					vIndex = tryParseInt(fAux);
					if (vIndex != -1) {
						ni[nv] = vIndex;
						tem_n = true;
					}

					faceScanner.close();

					// Prepara para proximo vertice
					nv++;
				}

				// Fim da face, aloca memoria para estruturas e preenche
				// com os valores lidos
				obj.faces[fcont].nv = nv;
				obj.faces[fcont].vert = new int[nv];

				// So aloca memoria para normais e texcoords se for
				// necessario
				if (tem_n) {
					obj.faces[fcont].norm = new int[nv];
				} else {
					obj.faces[fcont].norm = null;
				}

				if (tem_t) {
					obj.faces[fcont].tex = new int[nv];
				} else {
					obj.faces[fcont].tex = null;
				}

				// Copia os indices dos arrays temporarios para a face
				for (i = 0; i < nv; ++i) {

					// Subtrimos -1 dos indices porque o formato OBJ
					// comeca a contar a partir de 1, nao 0
					obj.faces[fcont].vert[i] = vi[i] - 1;

					if (tem_n) {
						obj.faces[fcont].norm[i] = ni[i] - 1;
					}

					if (tem_t) {
						obj.faces[fcont].tex[i] = ti[i] - 1;
					}
				}

				// Prepara para proxima face
				fcont++;
			}

			lineScanner.close();
		}

		newScnr.close();

		// Adiciona na lista
		// _objetos.push_back(obj);

		return obj;
	}

	private static int tryParseInt(String value) {
		try {
			int aux = Integer.parseInt(value);
			return aux;
		} catch (NumberFormatException nfe) {
			return -1;
		}
	}
}
