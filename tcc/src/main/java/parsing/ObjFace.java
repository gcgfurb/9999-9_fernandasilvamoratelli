package parsing;

public class ObjFace {

	int nv; // n�mero de v�rtices na face
	int[] vert; // �ndices dos v�rtices
	int[] norm; // �ndices das normais
	int[] tex; // �ndices das texcoords
	int mat; // �ndice para o material (se houver)
	int texid; // �ndice para a textura (se houver)

	public ObjFace() {

	}

	public int[] getVert() {
		return vert;
	}

	public void setVert(int[] vert) {
		this.vert = vert;
	}

	public int[] getNorm() {
		return norm;
	}

	public void setNorm(int[] norm) {
		this.norm = norm;
	}

}
