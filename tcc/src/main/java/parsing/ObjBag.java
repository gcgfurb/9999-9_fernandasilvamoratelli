package parsing;

import java.util.ArrayList;

public class ObjBag {

	int numVertices;
	int numFaces;
	int numNormais;
	int numTexcoords;
	boolean normais_por_vertice; // true se houver normais por v�rtice

	boolean tem_materiais; // true se houver materiais

	int textura; // cont�m a id da textura a utilizar, caso o objeto n�o tenha
					// textura associada
	int dlist; // display list, se houver

	// VERT *vertices;
	ObjVertex[] vertices;

	// VERT *normais;
	ObjVertex[] normais;

	// FACE *faces;
	ObjFace[] faces;

	// TEXCOORD *texcoords;

	public ObjBag() {
		// Inicializa contadores do objeto
		this.numVertices = 0;
		this.numFaces = 0;
		this.numNormais = 0;
		this.numTexcoords = 0;
		// A princ�pio n�o temos normais por v�rtice...
		this.normais_por_vertice = false;
		// E tamb�m n�o temos materiais...
		this.tem_materiais = false;
		this.textura = -1; // sem textura associada
		this.dlist = -1; // sem display list

		this.vertices = null;
		this.faces = null;
		this.normais = null;
		// obj.texcoords = null;
	}

	public ObjVertex[] getVertices() {
		return vertices;
	}

	public void setVertices(ObjVertex[] vertices) {
		this.vertices = vertices;
	}

	public ObjVertex[] getNormais() {
		return normais;
	}

	public void setNormais(ObjVertex[] normais) {
		this.normais = normais;
	}

	public ObjFace[] getFaces() {
		return faces;
	}

	public void setFaces(ObjFace[] faces) {
		this.faces = faces;
	}

}
