import javax.media.opengl.GL;
public final class ObjetoGrafico {
	GL gl;

	private int primitiva = GL.GL_TRIANGLES;
	
	private Ponto4D[] vertices = { 	
			new Ponto4D(5.0, 0.0, 0.0, 1.0),
			new Ponto4D(0.0, 0.0, 5.0, 1.0), 
			new Ponto4D(-5.0, 0.0, 0.0, 1.0) };

	private BoundingBox bbox = new BoundingBox();

	private Transformacao4D matrizObjeto = new Transformacao4D();
	private double angulo = 0.0;
	private double deslocamento = 4.0;
	
	public ObjetoGrafico() {
// Deslocamento inicial do Objeto		
//		Transformacao4D matrizTranslate = new Transformacao4D();
//		matrizTranslate.atribuirTranslacao(0,0,20);
//		matrizObjeto = matrizObjeto.transformacaoMatriz(matrizTranslate);
	}

	public void atribuirGL(GL gl) {
		this.gl = gl;
	}

	public void desenha() {
		gl.glColor3f(1.0f, 1.0f, 0.0f);
		gl.glLineWidth(2.0f);
		gl.glPointSize(2.0f);

		gl.glPushMatrix();
			gl.glMultMatrixd(matrizObjeto.obterDados(), 0);
			gl.glBegin(primitiva);
				for (byte i=0; i < vertices.length; i++) {
					gl.glVertex3d(vertices[i].obterX(), vertices[i].obterY(), vertices[i].obterZ());
				}
			gl.glEnd();
		gl.glPopMatrix();
		bbox.desenharOpenGLBBox(gl);
	}
	
	public void Animacao(int sorteio) {
		switch (sorteio) {
			case 0: ViraEsquerda(); break;
			case 1: ViraDireita(); break;
			case 2: Acelera(); break;
		default:
			System.out.println(sorteio);
			break;
		}
	}
	
	public void ViraEsquerda() {
		angulo += 2.0;
		System.out.println(angulo);
	}

	public void ViraDireita() {
		angulo -= 2.0;
		System.out.println(angulo);
	}
	
	public void Acelera() {
		rotacaoY(angulo);
		translacaoXYZ(0.0, 0.0, deslocamento);
	}
	
	public void Freia() {
		rotacaoY(-angulo);
		translacaoXYZ(0.0, 0.0, -deslocamento);
	}

	public void atribuirIdentidade() {
		angulo = 0.0;
		matrizObjeto.atribuirIdentidade();
	}
	
	public void translacaoXYZ(double tx, double ty, double tz) {
		Transformacao4D matrizTranslate = new Transformacao4D();
		matrizTranslate.atribuirTranslacao(tx,ty,tz);
		matrizObjeto = matrizObjeto.transformacaoMatriz(matrizTranslate);		
	}
	
	public void rotacaoY(double angulo) {
		Transformacao4D rotacaoY = new Transformacao4D();
		rotacaoY.atribuirRotacaoY(Transformacao4D.DEG_TO_RAD * angulo);
		matrizObjeto = matrizObjeto.transformacaoMatriz(rotacaoY);		
	}
	
	public void debug() {
		matrizObjeto.exibeMatriz();
	}
	
}

