import javax.media.opengl.GL;

public final class BoundingBox {
	private double menorX;
	private double menorY;
	private double menorZ;
	private double maiorX;
	private double maiorY;
	private double maiorZ;
	private Ponto4D centro = new Ponto4D();

	public BoundingBox() {
		this(0, 0, 0, 0, 0, 0);
	}
	
	public BoundingBox(double menorX, double menorY, double menorZ, double maiorX, double maiorY, double maiorZ) {
		atribuirBoundingBox(menorX,menorY,menorZ,maiorX,maiorY,maiorZ);
	}
	
	public void atribuirBoundingBox(double menorX, double menorY, double menorZ, double maiorX, double maiorY, double maiorZ) {
		this.menorX = menorX;
		this.menorY = menorY;
		this.menorZ = menorZ;
		this.maiorX = maiorX;
		this.maiorY = maiorY;
		this.maiorZ = maiorZ;
		processarCentroBBox();
	}
		
	public void atualizarBBox(Ponto4D ponto) {
	    atualizarBBox(ponto.obterX(),ponto.obterY(),ponto.obterZ());
	}

	public void atualizarBBox(double x, double y, double z) {
	    if (x < menorX)
	        menorX = x;
	    else {
	        if (x > maiorX) maiorX = x;
	    }
	    if (y < menorY)
	        menorY = y;
	    else {
	        if (y > maiorY) maiorY = y;
	    }
	    if (z < menorZ)
	        menorZ = z;
	    else {
	        if (z > maiorZ) maiorZ = z;
	    }
	}
	
	public void processarCentroBBox() {
	    centro.atribuirX((maiorX + menorX)/2);
	    centro.atribuirY((maiorY + menorY)/2);
	    centro.atribuirZ((maiorZ + menorZ)/2);
	}

	boolean testaPonto(Ponto4D p)
	{
	    if (p.obterX() < menorX)
	        return false;
	    if (p.obterX() > maiorX)
	        return false;
	    if (p.obterZ() < menorZ)
	        return false;
	    if (p.obterZ() > maiorZ)
	        return false;
	    if (p.obterY() < menorY)
	        return false;
	    if (p.obterY() > maiorY)
	        return false;
	    return true;
	}
	
	public void desenharOpenGLBBox(GL gl) {
		gl.glColor3f(1.0f, 0.0f, 0.0f);		
		gl.glBegin (GL.GL_LINE_LOOP);	// base
			gl.glVertex3d (menorX, menorY, menorZ);
			gl.glVertex3d (maiorX, menorY, menorZ);
			gl.glVertex3d (maiorX, menorY, maiorZ);
			gl.glVertex3d (menorX, menorY, maiorZ);
	    gl.glEnd();
	    gl.glBegin(GL.GL_LINE_LOOP);		// topo
			gl.glVertex3d (menorX, maiorY, menorZ);
			gl.glVertex3d (maiorX, maiorY, menorZ);
			gl.glVertex3d (maiorX, maiorY, maiorZ);
			gl.glVertex3d (menorX, maiorY, maiorZ);
	    gl.glEnd();
	    gl.glBegin(GL.GL_LINE_LOOP);		// frente
	    		gl.glVertex3d (menorX, menorY, maiorZ);
	    		gl.glVertex3d (menorX, maiorY, maiorZ);
	    		gl.glVertex3d (maiorX, maiorY, maiorZ);
	    		gl.glVertex3d (maiorX, menorY, maiorZ);
	    gl.glEnd();
	    gl.glBegin(GL.GL_LINE_LOOP);		// atras
			gl.glVertex3d (menorX, menorY, menorZ);
			gl.glVertex3d (menorX, maiorY, menorZ);
			gl.glVertex3d (maiorX, maiorY, menorZ);
			gl.glVertex3d (maiorX, menorY, menorZ);
		gl.glEnd();
	    gl.glBegin(GL.GL_LINE_LOOP);		// esquerda
			gl.glVertex3d (menorX, menorY, menorZ);
			gl.glVertex3d (menorX, menorY, maiorZ);
			gl.glVertex3d (menorX, maiorY, maiorZ);
			gl.glVertex3d (menorX, maiorY, menorZ);
		gl.glEnd();
	    gl.glBegin(GL.GL_LINE_LOOP);		// diretia
			gl.glVertex3d (maiorX, menorY, menorZ);
			gl.glVertex3d (maiorX, menorY, maiorZ);
			gl.glVertex3d (maiorX, maiorY, maiorZ);
			gl.glVertex3d (maiorX, maiorY, menorZ);
		gl.glEnd();
	}

	/// Obter menor valor X da BBox.
	public double obterMenorX() {
		return menorX;
	}

	/// Obter menor valor Y da BBox.
	public double obterMenorY() {
		return menorY;
	}

	/// Obter menor valor Z da BBox.
	public double obterMenorZ() {
		return menorZ;
	}

	/// Obter maior valor X da BBox.
	public double obterMaiorX() {
		return maiorX;
	}

	/// Obter maior valor Y da BBox.
	public double obterMaiorY() {
		return maiorY;
	}

	/// Obter maior valor Z da BBox.
	public double obterMaiorZ() {
		return maiorZ;
	}
	
	/// Obter ponto do centro da BBox.
	public Ponto4D obterCentro() {
		return centro;
	}

}

